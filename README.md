# PizzaApp by pkwan

### Getting started

`git clone https://gitlab.com/peterkwkwan/pizza-ordering-app.git` from an empty directory.

Git source can be found here: https://gitlab.com/peterkwkwan/pizza-ordering-app

### `npm i`

> Installs the necessary dependencies

### `npm run start`

> Runs the server in the development mode.
> Open [http://localhost:3000/](http://localhost:3000/) to view it in the browser.

### Tech

PizzaApp makes use of a number of tools, including:

- [@material-ui] - front-end UI framework
- [@reduxjs/toolkit] - makes using redux simpler
- [react] - front-end framework
- [react-redux] - for app-wide state management
