import { Typography } from "@material-ui/core";
import React from "react";

const Technologies = [
  { id: 1, name: "@material-ui" },
  { id: 2, name: "@reduxjs/toolkit" },
  { id: 3, name: "react" },
  { id: 4, name: "react-redux" },
];
export default function Footer() {
  return (
    <div>
      <Typography variant="overline">Technologies used</Typography>
      <div>
        {Technologies.map((tech) => {
          return (
            <Typography variant="subtitle2" key={tech.id}>
              {tech.name}
            </Typography>
          );
        })}
      </div>
      <Typography variant="overline">
        PizzaApp by peterkwkwan © twentytwentyone
      </Typography>
    </div>
  );
}
