import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Button, Typography, CircularProgress } from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";

import { useAppSelector, useAppDispatch } from "../../app/hooks";
import { removeFromCheckout, emptyCheckout } from "../Pizza/PizzaSlice";

const useStyles = makeStyles((theme) => ({
  deleteIcon: {
    cursor: "pointer",
    fill: "hsl(0, 55%, 50%)",
    "&:hover": {
      fill: "hsl(0, 85%, 75%)",
    },
  },
  checkoutBtn: {
    backgroundColor: "hsl(42, 100%, 71%)",
    fontWeight: 600,
    marginTop: "15px",
    width: "80%",
    minWidth: "150px",
    "&:disabled": {
      backgroundColor: "hsl(0, 0%, 85%)",
    },
    "&:hover": {
      color: "hsl(0, 0%, 85%)",
      backgroundColor: " hsl(0, 0%, 0.13%)",
    },
  },
}));

export default function Checkout() {
  const [processingOrder, setProcessingOrder] = useState(false);
  const { checkout } = useAppSelector((state) => state.pizza);
  const dispatch = useAppDispatch();
  const classes = useStyles();

  const getTotal = (): number => {
    let total: number = 0;
    for (let i = 0; i < checkout.length; i++) {
      total += checkout[i].price;
    }
    return total;
  };

  const submitCheckout = () => {
    setProcessingOrder(true);
    setTimeout(() => {
      dispatch(emptyCheckout());
      setProcessingOrder(false);
    }, 1500);
  };

  return (
    <div style={{ marginTop: "15px" }}>
      {!checkout.length ? (
        <Typography variant="subtitle1" style={{ fontFamily: "Cabin" }}>
          Add a pizza to your basket!
        </Typography>
      ) : (
        checkout?.map((item) => {
          return (
            <div key={item.id} style={{ margin: "25px 0" }}>
              <div
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  alignItems: "center",
                }}
              >
                <DeleteIcon
                  className={classes.deleteIcon}
                  onClick={() => {
                    dispatch(removeFromCheckout(item.id));
                  }}
                />

                <div>
                  <Typography variant="h5" style={{ fontFamily: "Cabin" }}>
                    {item.pizzaName}
                  </Typography>
                  <Typography variant="overline">
                    {item.size}, {item.topping}
                  </Typography>
                </div>
                <div>
                  <Typography variant="h5" style={{ fontFamily: "Cabin" }}>
                    ${item.price}
                  </Typography>
                </div>
              </div>
            </div>
          );
        })
      )}
      <div
        style={{ marginTop: "25px", borderTop: "2px solid hsl(0, 0%, 50%)" }}
      >
        <div
          style={{
            marginTop: "15px",
            display: "flex",
            justifyContent: "space-between",
          }}
        >
          <Typography variant="h4" style={{ fontFamily: "Cabin" }}>
            Total:
          </Typography>
          <Typography
            variant="h4"
            style={{ fontWeight: 600, fontFamily: "Cabin" }}
          >
            ${getTotal()}
          </Typography>
        </div>
        {processingOrder ? (
          <CircularProgress style={{ color: "hsl(42, 100%, 71%)" }} />
        ) : (
          <Button
            className={classes.checkoutBtn}
            variant="contained"
            disabled={!checkout.length}
            onClick={() => {
              submitCheckout();
            }}
          >
            Checkout
          </Button>
        )}
      </div>
    </div>
  );
}
