interface PizzaItem {
  pizzaId: number;
  pizzaName: string;
  price: number;
}

interface CheckoutItem {
  pizzaId: number;
  pizzaName: string;
  price: number;
  id: number;
  topping: string;
  size: string;
}

interface PizzaState {
  pizzaMenu: PizzaItem[];
  checkout: CheckoutItem[];
}

export const InitialState = {
  pizzaMenu: [
    {
      pizzaId: 1,
      pizzaName: "Cheese",
      price: 90,
    },
    {
      pizzaId: 2,
      pizzaName: "Pepperoni",
      price: 110,
    },
    {
      pizzaId: 3,
      pizzaName: "Deluxe",
      price: 120,
    },
    {
      pizzaId: 4,
      pizzaName: "Four Cheese",
      price: 110,
    },
    {
      pizzaId: 5,
      pizzaName: "Vegetarian",
      price: 100,
    },
    {
      pizzaId: 6,
      pizzaName: "Hawaiian",
      price: 110,
    },
  ],
  checkout: [],
} as PizzaState;
