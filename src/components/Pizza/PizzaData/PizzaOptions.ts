export const ToppingsChoices = [
  { id: 1, name: "Mushroom" },
  { id: 2, name: "Olives" },
  { id: 3, name: "Tomato" },
  { id: 4, name: "Extra Cheese" },
  { id: 5, name: "Pepper" },
];

export const SizeChoices = [
  { id: 1, name: 'Small (10")' },
  { id: 2, name: 'Medium (12")' },
  { id: 3, name: 'Large (14")' },
];
