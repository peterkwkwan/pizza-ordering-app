import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import { Button, CardMedia } from "@material-ui/core";

// components
import PizzaOptions from "../PizzaOptions";

type PizzaChoice = { pizzaId: Number; pizzaName: String; price: Number };

interface PizzaProps {
  pizzaChoice: PizzaChoice;
}

const useStyles = makeStyles((theme) => ({
  pizzaCard: {
    border: "5px solid hsl(42, 100%, 71%)",
    backgroundColor: "hsl(0, 0%, 90%)",
    boxShadow: "0 3px 10px rgb(0 0 0 / 0.2)",
    "& h5": {
      fontFamily: "Cabin",
    },
    "& button": {
      backgroundColor: "hsl(42, 100%, 71%)",
      fontWeight: 600,
    },
    "&:hover": {
      transform: "scale(1.01, 1.01)",
      backgroundColor: "hsl(0, 0%, 88%)",
      "& button": {
        color: "hsl(0, 0%, 85%)",
        backgroundColor: " hsl(0, 0%, 0.13%)",
      },
    },
  },
}));

export default function PizzaItem({ pizzaChoice }: PizzaProps) {
  const [open, setOpen] = React.useState(false);
  const classes = useStyles();

  const img = require(`../../../assets/images/${pizzaChoice.pizzaId}.jpg`);

  return (
    <Card className={classes.pizzaCard} style={{ margin: "10px" }}>
      <CardContent>
        <CardMedia
          style={{ width: "100%", height: "300px", objectFit: "cover" }}
          image={img.default}
          component="img"
          title={`${pizzaChoice.pizzaName}`}
        />
        <Typography variant="h5">{pizzaChoice.pizzaName}</Typography>
        <Typography variant="h5">${pizzaChoice.price}</Typography>
      </CardContent>
      <CardActions style={{ justifyContent: "center" }}>
        <Button variant="outlined" onClick={() => setOpen(true)}>
          Select
        </Button>
      </CardActions>
      <PizzaOptions open={open} setOpen={setOpen} pizzaChoice={pizzaChoice} />
    </Card>
  );
}
