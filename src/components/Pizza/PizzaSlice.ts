import { createSlice } from "@reduxjs/toolkit";
import { InitialState as initialState } from "./PizzaData/InitialState";

const pizzaSlice = createSlice({
  name: "pizza",
  initialState,
  reducers: {
    addToCheckout: (state, action) => {
      state.checkout.push(action.payload);
    },
    removeFromCheckout: (state, { payload }) => {
      state.checkout = state.checkout.filter(({ id }) => id !== payload);
    },
    emptyCheckout: (state) => {
      state.checkout = [];
    },
  },
});

export const { addToCheckout, removeFromCheckout, emptyCheckout } =
  pizzaSlice.actions;

export default pizzaSlice.reducer;
