import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  Modal,
  Button,
  Radio,
  RadioGroup,
  FormControlLabel,
  FormControl,
  FormLabel,
} from "@material-ui/core";

import { useAppDispatch } from "../../../app/hooks";
import { addToCheckout } from "../PizzaSlice";
import {
  ToppingsChoices as toppingsChoices,
  SizeChoices as sizeChoices,
} from "../PizzaData/PizzaOptions";

type PizzaChoice = { pizzaId: Number; pizzaName: String; price: Number };

interface ModalProps {
  open: boolean;
  setOpen: any;
  pizzaChoice: PizzaChoice;
}

const useStyles = makeStyles((theme) => ({
  modal: {
    width: 400,
    backgroundColor: "hsl(0,0%,96%)",
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
  addToBasket: {
    backgroundColor: "hsl(42, 100%, 71%)",
    fontWeight: 600,
    "&:disabled": {
      backgroundColor: "hsl(0, 0%, 85%)",
    },
    "&:hover": {
      color: "hsl(0, 0%, 85%)",
      backgroundColor: " hsl(0, 0%, 0.13%)",
    },
  },
}));

export default function PizzaOptions({
  open,
  setOpen,
  pizzaChoice,
}: ModalProps) {
  const classes = useStyles();
  const dispatch = useAppDispatch();

  const [topping, setTopping] = React.useState("");
  const [size, setSize] = React.useState("");

  const handleClose = () => {
    setOpen(false);
  };

  const handleToppingChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setTopping(event.target.value);
  };
  const handleSizeChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSize(event.target.value);
  };

  const addToBasket = () => {
    dispatch(
      addToCheckout({
        ...pizzaChoice,
        topping: topping,
        size: size,
        id: generateUniqueId(),
      })
    );
    setTopping("");
    setSize("");
    handleClose();
  };

  function generateUniqueId() {
    let S4 = function () {
      return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    };
    return `${S4()}${S4()}-${S4()}-${S4()}-${S4()}${S4()}`;
  }

  return (
    <Modal
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
      open={open}
      onClose={handleClose}
    >
      <div className={classes.modal}>
        <FormControl component="fieldset">
          <FormLabel component="legend">Topping</FormLabel>
          <RadioGroup
            aria-label="topping"
            name="topping"
            value={topping}
            onChange={handleToppingChange}
          >
            {toppingsChoices.map((topping) => {
              return (
                <FormControlLabel
                  key={topping.id}
                  value={topping.name}
                  control={<Radio />}
                  label={topping.name}
                />
              );
            })}
          </RadioGroup>
        </FormControl>
        <FormControl component="fieldset">
          <FormLabel component="legend">Size</FormLabel>
          <RadioGroup
            aria-label="size"
            name="size"
            value={size}
            onChange={handleSizeChange}
          >
            {sizeChoices.map((size) => {
              return (
                <FormControlLabel
                  key={size.id}
                  value={size.name}
                  control={<Radio />}
                  label={size.name}
                />
              );
            })}
          </RadioGroup>
        </FormControl>
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            marginTop: "15px",
          }}
        >
          <Button
            variant="contained"
            className={classes.addToBasket}
            disabled={!topping || !size}
            onClick={() => {
              addToBasket();
            }}
          >
            Add to basket
          </Button>
          <Button variant="outlined" color="default" onClick={handleClose}>
            Cancel
          </Button>
        </div>
      </div>
    </Modal>
  );
}
