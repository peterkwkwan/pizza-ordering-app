import React from "react";
import { useAppSelector } from "../../../app/hooks";
import PizzaItem from "../PizzaItem";

export default function PizzaMenu() {
  const { pizzaMenu } = useAppSelector((state) => state.pizza);

  return (
    <div
      style={{
        height: "100%",
        display: "flex",
        flexWrap: "wrap",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      {pizzaMenu.map((pizzaChoice) => {
        return (
          <PizzaItem key={pizzaChoice.pizzaId} pizzaChoice={pizzaChoice} />
        );
      })}
    </div>
  );
}
