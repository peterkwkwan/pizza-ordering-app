import React from "react";
import { store } from "./app/store";
import { Provider } from "react-redux";
import "./App.css";

// components
import PizzaMenu from "./components/Pizza/PizzaMenu";
import Checkout from "./components/Checkout";
import Footer from "./components/Footer";

function App() {
  return (
    <Provider store={store}>
      <div className="App">
        <PizzaMenu />
        <div
          style={{
            width: "620px",
            padding: "20px",
            margin: "15px",
            backgroundColor: "hsl(0, 0%, 97%)",
            display: "flex",
            flexDirection: "column",
            justifyContent: "space-between",
          }}
        >
          <Checkout />
          <Footer />
        </div>
      </div>
    </Provider>
  );
}

export default App;
